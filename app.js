// https://peachscript.github.io/vue-infinite-loading/#!/
//
var root = 'https://jsonplaceholder.typicode.com';

var restVue = new Vue({
    el: '#app',
    data: {
        name: '',
        userData: [],

        rdvs:
            [
                {
                    'titreRdv': 'Ophtalmo',
                    'contact': 'Dr Tatz',
                    'lieux': '8 place Carnot, 69002 Lyon',
                    'date': 'vendredi 2 mars',
                    'dateDay': '2',
                    'mois': 'mars',
                    'dateDayText': 'FRI',
                    'heureDebut': '14:00',
                    'heureFin': '14:30',
                    'telephone': '0695410018',
                    'image': 'resources/img/none.png',
                    'theme': 'none2'
                },
                {
                    'titreRdv': 'Apéro',
                    'contact': 'Adrien',
                    'lieux': 'Bar',
                    'date': 'lundi 7 mars',
                    'dateDay': '7',
                    'mois': 'mars',
                    'dateDayText': 'MON',
                    'heureDebut': '14:00',
                    'heureFin': '16:00',
                    'telephone': '0695410018',
                    'image': 'resources/img/pro.jpg',
                    'theme': 'pro'
                },
                {
                    'titreRdv': 'Soirée St Valentin',
                    'contact': 'Lou',
                    'lieux': 'Maison',
                    'date': 'mercredi 9 mars',
                    'dateDay': '9',
                    'mois': 'mars',
                    'dateDayText': 'THU',
                    'heureDebut': '20:00',
                    'heureFin': '00:00',
                    'telephone': '',
                    'image': 'resources/img/perso.jpg',
                    'theme': 'perso'
                },
                {
                    'titreRdv': 'Pitch',
                    'contact': 'Jean',
                    'lieux': 'Tatz',
                    'date': 'jeudi 10 mars',
                    'dateDay': '10',
                    'mois': 'mars',
                    'dateDayText': 'THU',
                    'heureDebut': '17:00',
                    'heureFin': '18:00',
                    'telephone': '0695410018',
                    'image': 'resources/img/public.jpg',
                    'theme': 'public'
                },

            {
                'titreRdv': 'RDV banque',
                'contact': 'Me Perrin',
                'lieux': 'Socgen Agence Bellecour',
                'date': 'mercredi 31 mars',
                'dateDay': '31',
                'mois': 'mars',
                'dateDayText': 'WED',
                'heureDebut': '11:00',
                'heureFin': '12:00',
                'telephone': '0695410018',
                'image': 'resources/img/none.png',
                'theme': 'none'
            },
                {
                'titreRdv': 'Ophtalmo',
                'contact': 'Dr Tatz',
                'lieux': '8 place Carnot, 69002 Lyon',
                'date': 'vendredi 2 avril',
                'dateDay': '2',
                'mois': 'avril',
                'dateDayText': 'FRI',
                'heureDebut': '14:00',
                'heureFin': '14:30',
                'telephone': '0695410018',
                'image': 'resources/img/none.png',
                'theme': 'none2'
            },
                {
                'titreRdv': 'Apéro',
                'contact': 'Adrien',
                'lieux': 'Bar',
                'date': 'lundi 7 avril',
                'dateDay': '7',
                    'mois': 'avril',
                'dateDayText': 'MON',
                'heureDebut': '14:00',
                'heureFin': '16:00',
                'telephone': '0695410018',
                'image': 'resources/img/pro.jpg',
                'theme': 'pro'
            },
            {
                'titreRdv': 'Soirée St Valentin',
                'contact': 'Lou',
                'lieux': 'Maison',
                'date': 'mercredi 9 avril',
                'dateDay': '9',
                'mois': 'avril',
                'dateDayText': 'THU',
                'heureDebut': '20:00',
                'heureFin': '00:00',
                'telephone': '',
                'image': 'resources/img/perso.jpg',
                'theme': 'perso'
            },
            {
                'titreRdv': 'Pitch',
                'contact': 'Jean',
                'lieux': 'Tatz',
                'date': 'jeudi 10 avril',
                'dateDay': '10',
                'mois': 'avril',
                'dateDayText': 'THU',
                'heureDebut': '17:00',
                'heureFin': '18:00',
                'telephone': '0695410018',
                'image': 'resources/img/public.jpg',
                'theme': 'public'
            },
                {
                    'titreRdv': 'Ophtalmo',
                    'contact': 'Dr Tatz',
                    'lieux': '8 place Carnot, 69002 Lyon',
                    'date': 'vendredi 2 mars',
                    'dateDay': '2',
                    'mois': 'mars',
                    'dateDayText': 'FRI',
                    'heureDebut': '14:00',
                    'heureFin': '14:30',
                    'telephone': '0695410018',
                    'image': 'resources/img/none.png',
                    'theme': 'none2'
                },
                {
                    'titreRdv': 'Apéro',
                    'contact': 'Adrien',
                    'lieux': 'Bar',
                    'date': 'lundi 7 mars',
                    'dateDay': '7',
                    'mois': 'mars',
                    'dateDayText': 'MON',
                    'heureDebut': '14:00',
                    'heureFin': '16:00',
                    'telephone': '0695410018',
                    'image': 'resources/img/pro.jpg',
                    'theme': 'pro'
                },
                {
                    'titreRdv': 'Soirée St Valentin',
                    'contact': 'Lou',
                    'lieux': 'Maison',
                    'date': 'mercredi 9 mars',
                    'dateDay': '9',
                    'mois': 'mars',
                    'dateDayText': 'THU',
                    'heureDebut': '20:00',
                    'heureFin': '00:00',
                    'telephone': '',
                    'image': 'resources/img/perso.jpg',
                    'theme': 'perso'
                },
                {
                    'titreRdv': 'Pitch',
                    'contact': 'Jean',
                    'lieux': 'Tatz',
                    'date': 'jeudi 10 mars',
                    'dateDay': '10',
                    'mois': 'mars',
                    'dateDayText': 'THU',
                    'heureDebut': '17:00',
                    'heureFin': '18:00',
                    'telephone': '0695410018',
                    'image': 'resources/img/public.jpg',
                    'theme': 'public'
                },

                {
                    'titreRdv': 'RDV banque',
                    'contact': 'Me Perrin',
                    'lieux': 'Socgen Agence Bellecour',
                    'date': 'mercredi 31 mars',
                    'dateDay': '31',
                    'mois': 'mars',
                    'dateDayText': 'WED',
                    'heureDebut': '11:00',
                    'heureFin': '12:00',
                    'telephone': '0695410018',
                    'image': 'resources/img/none.png',
                    'theme': 'none'
                },
                {
                    'titreRdv': 'Ophtalmo',
                    'contact': 'Dr Tatz',
                    'lieux': '8 place Carnot, 69002 Lyon',
                    'date': 'vendredi 2 avril',
                    'dateDay': '2',
                    'mois': 'avril',
                    'dateDayText': 'FRI',
                    'heureDebut': '14:00',
                    'heureFin': '14:30',
                    'telephone': '0695410018',
                    'image': 'resources/img/none.png',
                    'theme': 'none2'
                },
                {
                    'titreRdv': 'Apéro',
                    'contact': 'Adrien',
                    'lieux': 'Bar',
                    'date': 'lundi 7 avril',
                    'dateDay': '7',
                    'mois': 'avril',
                    'dateDayText': 'MON',
                    'heureDebut': '14:00',
                    'heureFin': '16:00',
                    'telephone': '0695410018',
                    'image': 'resources/img/pro.jpg',
                    'theme': 'pro'
                },
                {
                    'titreRdv': 'Soirée St Valentin',
                    'contact': 'Lou',
                    'lieux': 'Maison',
                    'date': 'mercredi 9 avril',
                    'dateDay': '9',
                    'mois': 'avril',
                    'dateDayText': 'THU',
                    'heureDebut': '20:00',
                    'heureFin': '00:00',
                    'telephone': '',
                    'image': 'resources/img/perso.jpg',
                    'theme': 'perso'
                },
                {
                    'titreRdv': 'Pitch',
                    'contact': 'Jean',
                    'lieux': 'Tatz',
                    'date': 'jeudi 10 avril',
                    'dateDay': '10',
                    'mois': 'avril',
                    'dateDayText': 'THU',
                    'heureDebut': '17:00',
                    'heureFin': '18:00',
                    'telephone': '0695410018',
                    'image': 'resources/img/public.jpg',
                    'theme': 'public'
                }
        ],
        details: [],
        comments: [],
        statusData: []
    },
    // define methods under the `methods` object
    methods: {
        getDataFromUserName: function (event) {
            if (event) {
                axios.get('https://api.github.com/users/'+this.name).then(response => this.userData = response.data);
            }
        },
        getComments: function (event) {
            // `this` inside methods points to the Vue instance
            // `event` is the native DOM event
            if (event) {
                axios.get(root + '/comments').then(response=>this.comments = response.data);
            }
        },
        afficher: function (index) {
            // `this` inside methods points to the Vue instance
            // `event` is the native DOM event
                this.details.pop(0);
                this.details.push(this.rdvs[index]);
                console.log(this.rdvs[index]);

//            alert(this.rdvs[index].lieux);
            //rdvs[index]);
        },


        // POST REQUEST
        postData: function (event) {
            if (event) {
                axios.post(root + '/posts', {title: 'foo', body: this.name, userId: 1})
                // .then(response => console.log(response.statusText))
                    .then(response => this.statusData = response.statusText);
            }
        }

    }

});